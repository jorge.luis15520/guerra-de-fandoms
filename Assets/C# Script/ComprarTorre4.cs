using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ComprarTorre4 : MonoBehaviour , IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    public bool colocartorre;
    public GameObject t2, t3, t4;
    private bool c2, c3, c4;
    public GameObject torre;
    public GameObject torreGhost;
    private GameObject plataforma = null;
    private GameObject torreActual;

    public GameObject hud;
    public GameObject stats;
    private bool active = false;
    private int oro;
    private Transform punto;
    public Texture2D cursorVender;
    public Texture2D cursorDefecto;
    private GameObject ghost;
    private bool drag = false;
    private Image image;
    public void OnPointerClick(PointerEventData eventData)
    {
        if (oro >= 300)
        {
            colocartorre = true;
        }

    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!colocartorre && !c2 && !c3 && !c4)
        {
            active = true;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        active = false;
    }


    // Start is called before the first frame update
    void Start()
    {
        colocartorre = false;
        image = GetComponent<Image>();

    }

    // Update is called once per frame
    void Update()
    {
        c2 = t2.GetComponent<ComprarTorre>().colocartorre;
        c3 = t3.GetComponent<ComprarTorre2>().colocartorre;
        c4 = t4.GetComponent<ComprarTorre3>().colocartorre;

        oro = hud.GetComponent<HUDModelo>().oro;

        if (colocartorre && oro >= 300)
        {
            Cursor.SetCursor(cursorVender, Vector2.zero, CursorMode.Auto);
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (!drag)
                {
                    ghost = (GameObject)Instantiate(torreGhost, hit.point, Quaternion.identity);
                    drag = true;
                }

                ghost.transform.position = hit.point;

                Debug.DrawLine(ray.origin, hit.point, Color.green);

                if (hit.transform.gameObject.tag == "Plataforma")
                {
                    plataforma = hit.transform.gameObject;
                    torreActual = plataforma.GetComponent<Plataforma>().torre;
                    punto = plataforma.GetComponent<Plataforma>().puntoCreacion;

                    if (Input.GetMouseButtonDown(0) && plataforma.tag == "Plataforma" && torreActual == null)
                    {
                        plataforma.GetComponent<Plataforma>().torre = (GameObject)Instantiate(torre, punto.position, punto.rotation);
                        hud.GetComponent<HUDModelo>().oro -= 300;
                        colocartorre = false;
                        Cursor.SetCursor(cursorDefecto, Vector2.zero, CursorMode.Auto);
                        Destroy(ghost);
                        drag = false;
                        active = false;

                    }
                }
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            colocartorre = false;
            Cursor.SetCursor(cursorDefecto, Vector2.zero, CursorMode.Auto);
            Destroy(ghost);
            drag = false;
            active = false;

        }


        if (oro < 300)
        {
            image.color = Color.grey;
            active = false;
        }
        else
        {
            image.color = Color.white;
        }

        if (active)
        {
            stats.SetActive(true);
        }
        else
        {
            stats.SetActive(false);
        }
    }
}
