using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour
{
    private Transform objetivo;
    public float velocidad;
    public int da�o;

    public bool da�oArea;
    public int rango;
    public List<GameObject> enemigosenRango = new List<GameObject>();

    private string tagEnemigo = "Enemigo";

    public void Buscar(Transform objetivo)
    {
        this.objetivo = objetivo;
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (objetivo == null )
        {
            Destroy(gameObject);
            return;
        }

        Vector3 direcci�n = objetivo.position - transform.position;
        transform.LookAt(objetivo);
        float distancia = velocidad * Time.deltaTime;

        if ( !da�oArea && direcci�n.magnitude <= distancia)
        {
            Da�o(objetivo);
            GolpearObjetivo();
            return;
        }

        if (da�oArea && direcci�n.magnitude <= distancia)
        {
            DetectarEnemigos();
            Da�oArea();
            GolpearObjetivo();
            return;
        }

        transform.Translate(direcci�n.normalized * distancia, Space.World);
    }

    void Da�o(Transform enemigo)
    {
        if (!da�oArea)
        {
            EnemigoControlador e = enemigo.GetComponent<EnemigoControlador>();

            if (enemigo != null)
            {
                e.RecibirDa�o(da�o);
            }
        }

        if (da�oArea)
        {
            if (enemigo != null)
            {
                for (int i = 0; i < enemigosenRango.Count; i++)
                {
                    enemigosenRango[i].GetComponent<EnemigoControlador>().RecibirDa�o(da�o);
                }
            }
        }
    }

    void Da�oArea()
    {
        if (da�oArea)
        {
           for (int i = 0; i < enemigosenRango.Count; i++)
           {
              enemigosenRango[i].GetComponent<EnemigoControlador>().RecibirDa�o(da�o);
           }
            
        }
    }

    void GolpearObjetivo()
    {
        Destroy(gameObject);
    }

    void DetectarEnemigos()
    {
       enemigosenRango.Clear();

       GameObject[] enemigos = GameObject.FindGameObjectsWithTag(tagEnemigo);

       foreach (GameObject enemigo in enemigos)
       {
          float distanciaDelEnemigo = Vector3.Distance(transform.position, enemigo.transform.position);

          if (distanciaDelEnemigo < rango)
          {
                    enemigosenRango.Add(enemigo);
          }
       }
        
    }

    void OnDrawGizmosSelected()
    {
        if (da�oArea)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, rango);
        }
    }
}
