using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoostJefe : MonoBehaviour
{
    public float rango;
    private string tagEnemigo = "Enemigo";
    public List<GameObject> enemigosenRango = new List<GameObject>();
    public bool boostLanzado = false;
    public float timer;

    // Start is called before the first frame update
    void Start()
    {

    }

    
    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        DetectarEnemigos();
        Boost();
    }

    void DetectarEnemigos()
    {
        if (!boostLanzado)
        {
            enemigosenRango.Clear();


            GameObject[] enemigos = GameObject.FindGameObjectsWithTag(tagEnemigo);

            foreach (GameObject enemigo in enemigos)
            {
                float distanciaDelEnemigo = Vector3.Distance(transform.position, enemigo.transform.position);
                if (distanciaDelEnemigo < rango)
                {
                    enemigosenRango.Add(enemigo);
                }
            }
        }
    }

    void Boost()
    {
        if (timer > 5 && !boostLanzado)
        {
            timer = 0;

            for (int i = 0; i < enemigosenRango.Count; i++)
            {
                enemigosenRango[i].GetComponent<EnemigoModelo>().velocidad += 2; 
            }

            boostLanzado = true;
        }
    }


    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, rango);
    }
}
