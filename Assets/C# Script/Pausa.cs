using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pausa : MonoBehaviour
{
    public static bool enPausa = false;
    public GameObject panelPausa;
    public List<OleadasController> oleadas = new List<OleadasController>();
    public int contador = 0;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Pausar()
    {
        panelPausa.SetActive(true);
        Time.timeScale = 0f;
        enPausa = true;
    }

    public void Volver()
    {
        panelPausa.SetActive(false);
        Time.timeScale = 1f;
        enPausa = false;
    }

    public void Reiniciar()
    {
        enPausa = false;
        Time.timeScale = 1f;
        panelPausa.SetActive(false);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Menu()
    {
        enPausa = false;
        Time.timeScale = 1f;
        SceneManager.LoadScene(0);
    }

    public void Velocidad()
    {
        contador++;

        if (contador > 2)
        {
            contador = 0;
        }

        switch (contador)
        {
            case 1:
                Time.timeScale = 2f;
                break;
            case 2:
                Time.timeScale = 3f;
                break;

            default:
                Time.timeScale = 1f;
                break;
        }

    }
}
