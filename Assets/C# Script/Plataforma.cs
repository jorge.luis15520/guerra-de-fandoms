using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plataforma : MonoBehaviour
{
    public GameObject torre;
    public Transform puntoCreacion;
    private Renderer render;
    private GameObject tutorial;

    private GameObject torre1, torre2, torre3, torre4;
    private bool t1, t2, t3, t4;

    public int contador;
    // Start is called before the first frame update
    private void Awake()
    {
        render = gameObject.GetComponent<Renderer>();
        tutorial = GameObject.FindGameObjectWithTag("tutorial");
        torre1 = GameObject.Find("Torre 1");
        torre2 = GameObject.Find("Torre 2");
        torre3 = GameObject.Find("Torre 3");
        torre4 = GameObject.Find("Torre 4");
    }

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (torre1 != null)
        {
            t1 = torre1.GetComponent<ComprarTorre>().colocartorre;
        }

        if (torre2 != null)
        {
            t2 = torre2.GetComponent<ComprarTorre2>().colocartorre;
        }

        if (torre3 != null)
        {
            t3 = torre3.GetComponent<ComprarTorre3>().colocartorre;
        }

        if (torre4 != null)
        {
            t4 = torre4.GetComponent<ComprarTorre4>().colocartorre;
        }

        if (torre != null)
        {
            CambioColor();
        }

        if (torre == null)
        {
            if (!t1 && !t2 && !t3 && !t4)
            {
                render.material.color = new Color(1f, 1f, 1f, 0.14f);
            }
        }
    }

    public void CambioColor()
    {
        render.material.color = new Color(0.13f, 1f, 0f, 0.47f);
    }

    private void OnMouseEnter()
    {
        if (t1 || t2|| t3 || t4)
        {
            CambioColor();
        }
    }
    private void OnMouseExit()
    {
        if (t1 || t2 || t3 || t4)
        {
            if (torre == null)
            {
                render.material.color = new Color(1f, 1f, 1f, 0.14f);
            }
        }
    }
}
