using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HudControlador : MonoBehaviour
{
    public int oro = 100;
    public Text oroTexto;
    public Text oleadaTexto;
    private GameObject oleadas;
    public GameObject vida1, vida2, vida3;
    private GameObject baseJugador;
    private int num;
    private int num2;



    // Start is called before the first frame update
    void Start()
    {
        oleadas = GameObject.FindGameObjectWithTag("Oleadas");
        baseJugador = GameObject.FindGameObjectWithTag("Base");

        vida2.SetActive(false);
        vida3.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        baseJugador = GameObject.FindGameObjectWithTag("Base");
        num = oleadas.GetComponent<OleadasController>().oleada.Count;
        num2 = oleadas.GetComponent<OleadasController>().oleadaActual;
        var vida = baseJugador.GetComponent<Base>().vida;
        
        oroTexto.text = "" + oro;
        oleadaTexto.text = "" + ((num-1) - num2);

        if (vida <= 4)
        {
            vida1.SetActive(false);
            vida2.SetActive(true);
            vida3.SetActive(false);
        }

        if (vida <= 2)
        {
            vida1.SetActive(false);
            vida2.SetActive(false);
            vida3.SetActive(true);
        }

    }
}
