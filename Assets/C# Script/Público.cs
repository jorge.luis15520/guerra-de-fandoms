using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Público : MonoBehaviour
{
    public List<Transform> puntosRuta = new List<Transform>();
    public List<Transform> puestos = new List<Transform>();
    public int velocidadRotacion;
    public float velocidad;
    public int objetivo = 1;
    public float tiempodeEspera;
    public float timer;
    private float velocidadIncial;
    public bool once = false;
    private int n;
    public float distance;
    private bool x;

    // Start is called before the first frame update
    void Awake()
    {
        ObtenerRuta();
    }

    void Start()
    {
        velocidadIncial = velocidad;
        x = false;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (once)
        {
            timer += Time.deltaTime;

            var dir = puestos[n].position - transform.position;

            var giro = Quaternion.LookRotation(dir);

            transform.rotation = Quaternion.Slerp(transform.rotation, giro, velocidadRotacion * Time.deltaTime);

            distance = Vector3.Distance(transform.position, puestos[n].position);

            if (timer >= tiempodeEspera)
            {
                timer = 0f;
                velocidad = velocidadIncial;
            }

            if (distance > 3.1f)
            {
                once = false;
            }
        }
        else
        {
            Distancia();
        }


        Movimiento();


        if (!once)
        {
            Girar();
        }

        if (objetivo == puntosRuta.Count - 1)
        {
            objetivo = 0;
        }
    }

    private void Movimiento()
    {

        transform.position = Vector3.MoveTowards(transform.position, puntosRuta[objetivo].position, velocidad * Time.deltaTime);
        var distancia = Vector3.Distance(transform.position, puntosRuta[objetivo].position);

        if (distancia <= 0.1f)
        {
            if (objetivo >= puntosRuta.Count - 1)
            {
                return;
            }

            objetivo++;
        }

    }

    private void Girar()
    {
        var dir = puntosRuta[objetivo].position - transform.position;

        var giro = Quaternion.LookRotation(dir);

        transform.rotation = Quaternion.Slerp(transform.rotation, giro, velocidadRotacion * Time.deltaTime);
    }

    private void ObtenerRuta()
    {
        var ruta = GameObject.Find("RutaPublico").transform;

        for (int i = 0; i < ruta.childCount; i++)
        {
            puntosRuta.Add(ruta.GetChild(i));
        }

        GameObject[] p = GameObject.FindGameObjectsWithTag("Puesto");

        for (int i = 0; i < p.Length; i++)
        {
            puestos.Add(p[i].transform);
        }
    }

    private void Distancia()
    {
        for (int i = 0; i < puestos.Count; i++)
        {
            distance = Vector3.Distance(transform.position, puestos[i].position);

            if (distance < 3f && !x)
            {
                once = true;
                velocidad = 0;
                n = i;
                x = true;
            }

            if (distance > 3f)
            {
                x = false;
            }
        }
    }
}
