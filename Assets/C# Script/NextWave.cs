using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NextWave : MonoBehaviour
{
    public OleadasController spawn;
    private bool siguiente;
    public GameObject boton;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {   
                if (hit.transform.tag == "NextWave")
                {
                    spawn.CambioOleada();
                }
            }
        }
    }
}
