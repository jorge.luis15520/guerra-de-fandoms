using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Base : MonoBehaviour
{
    public Image barraVida;

    public float vidaInicial;
    public float vida;

    private void Awake()
    {
        vida = vidaInicial;
    }
    void Start()
    {
        // vida = vidaInicial;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RecibirDa�o(float da�o)
    {

        vida -= da�o;

        barraVida.fillAmount = vida / vidaInicial;
    }
}
