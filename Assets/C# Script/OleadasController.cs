using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OleadasController : MonoBehaviour
{
    public List<EnemigosOleada> oleada = new List<EnemigosOleada>();
    public Transform puntoCreación;
    public bool siguienteOleada;
    public bool finOleada;
    public int oleadaActual;

    public Text tiempoOleada;
    private int tiempo;

    public GameObject boton;


    private void Start()
    {
        StartCoroutine(ProcesWave());
    }
    private void Update()
    {
        CheckSiguienteOleada();
        Boton();

    }
    private void CheckSiguienteOleada()
    {
        if (siguienteOleada && !finOleada)
        {

            oleada[oleadaActual].contador -= 1 * Time.deltaTime;
            tiempo = (int)oleada[oleadaActual].contador;
            tiempoOleada.text = "" + tiempo;

            if (oleada[oleadaActual].contador <= 0)
            {
                CambioOleada();
            }
        }
    }

    public void CambioOleada()
    {
        if (finOleada)
            return;
        oleadaActual++;
        StartCoroutine(ProcesWave());
    }

    public IEnumerator ProcesWave()
    {
        if (finOleada)
            yield break;
        siguienteOleada = false;
        oleada[oleadaActual].contador = oleada[oleadaActual].tiempoSiguienteOleada;
        for (int i = 0; i < oleada[oleadaActual].enemigos.Count; i++)
        {
            var spawn = Instantiate(oleada[oleadaActual].enemigos[i], puntoCreación.position, puntoCreación.rotation);
            yield return new WaitForSeconds(oleada[oleadaActual].tiempoEntreCreacion);
        }

        siguienteOleada = true;

        if (oleadaActual >= oleada.Count - 1)
        {
            finOleada = true;
        }



    }

    private void Boton()
    {
        if (!finOleada)
        {
            boton.SetActive(siguienteOleada);
        }
    }
}

    [System.Serializable]
    public class EnemigosOleada
    {
        public float tiempoEntreCreacion;
        public float tiempoSiguienteOleada;
        public float contador = 0;
        public List<EnemigoControlador> enemigos = new List<EnemigoControlador>();
    }

