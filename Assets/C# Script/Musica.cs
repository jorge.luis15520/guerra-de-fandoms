using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Musica : MonoBehaviour
{

    private AudioSource audioSource;
    public GameObject victoria;
    private bool v, d;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        v = victoria.GetComponent<VictoryController>().victoria;
        d = victoria.GetComponent<DefeatController>().derrota;

        if (Time.timeScale <= 0f && audioSource.isPlaying)
        {
            audioSource.Stop();
        }

        if (Time.time >= 1f && !audioSource.isPlaying)
        {
            audioSource.Play();
        }

        if (v && audioSource.isPlaying)
        {
            audioSource.Stop();
        }

        if (d && audioSource.isPlaying)
        {
            audioSource.Stop();
        }
    }
}
