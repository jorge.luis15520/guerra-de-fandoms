using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
    public List<GameObject> plataformas = new List<GameObject>();
    public GameObject oleada;
    public GameObject comprar;
    public GameObject vender;
    public GameObject combinar;
    public GameObject zoom;
    public GameObject torre2,vender2;
    public GameObject empezar;
    public Image t2, pala;
    public GameObject ayuda, preparate;
    public Camera c�mara;
    public bool compra = false;
    public bool venta = false;
    public bool combina = false;
    public bool zoom1 = false;
    private bool tutorial = true;
    private bool once = false;
    private bool once1 = false;
    private bool once2 = false;
    private float fov;

    private GameObject p;
    // Start is called before the first frame update

    private void Awake()
    {
        oleada.GetComponent<OleadasController>().enabled = false;
        torre2.GetComponent<ComprarTorre2>().enabled = false;
        vender2.GetComponent<VentaTorres>().enabled = false;
        t2.color = Color.gray;
        pala.color = Color.gray;
        ayuda.SetActive(false);
        fov = c�mara.GetComponent<CamaraController>().FOVmax;
        StartCoroutine(Test());
    }
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (tutorial && !compra)
        {
            for (int i = 0; i < plataformas.Count; i++)
            {
                if (plataformas[i].GetComponent<Plataforma>().torre != null)
                {
                    compra = true;
                }
            }
        }
        
        if (compra && !once)
        {
            StartCoroutine(Test());
            //comprar.SetActive(false);
            //ayuda.SetActive(false);
            //combinar.SetActive(true);
            //once = true;
        }

        if (combina && !once1)
        {
            StartCoroutine(Test());

            //combinar.SetActive(false);
            //vender.SetActive(true);
            //ayuda.SetActive(false);
            //once1 = true;
        }

        if (venta && !once2)
        {
            StartCoroutine(Test());

            //vender.SetActive(false);
            //zoom.SetActive(true);
            //once2 = true;
            //oleada.GetComponent<OleadasController>().enabled = true;
            //candado1.SetActive(false);
            //candado2.SetActive(false);
            //candado3.SetActive(false);
            //torre2.GetComponent<ComprarTorre2>().enabled = true;
            //torre3.GetComponent<ComprarTorre3>().enabled = true;
            //torre4.GetComponent<ComprarTorre4>().enabled = true;
            //ayuda.SetActive(false);

        }

        if (zoom1)
        {
            StartCoroutine(Test());

            if (preparate != null)
            {
                preparate.SetActive(true);
                Destroy(preparate, 3f);
            }

            zoom.SetActive(false);
            once2 = true;
            oleada.GetComponent<OleadasController>().enabled = true;
            torre2.GetComponent<ComprarTorre2>().enabled = true;
            t2.color = Color.white;
            ayuda.SetActive(false);
            tutorial = false;
        }

        if (tutorial && compra)
        {
            for (int i = 0; i < plataformas.Count; i++)
            {
                if (plataformas[i].GetComponent<Plataforma>().torre != null)
                {
                    p = plataformas[i];
                }

                if (plataformas[i].GetComponent<Plataforma>().torre == null && !combina)
                {
                    plataformas[i].SetActive(false);
                }
            }

            if (p != null)
            {

                if (p.GetComponent<Plataforma>().torre == null)
                {
                    venta = true;

                    for (int i = 0; i < plataformas.Count; i++)
                    {
                        plataformas[i].SetActive(true);
                    }
                }

                if (p.GetComponent<Plataforma>().torre != null)
                {
                     if (p.GetComponent<Plataforma>().torre.tag == "Torreta 2" && tutorial)
                     {
                         combina = true;
                         vender2.GetComponent<VentaTorres>().enabled = true;
                         pala.color = Color.white;   
                     }
                }

                if (venta)
                {
                    if (c�mara.fieldOfView < fov)
                    {
                        zoom1 = true;
                    }
                }

            }
        }
    }

    public void Continuar()
    {
        if (compra)
        {
            if (!combina)
            {
                combinar.SetActive(false);
                ayuda.SetActive(true);
            }
            else
            {
                if (!venta)
                {
                    ayuda.SetActive(true);
                    vender.SetActive(false);
                }
                else
                {
                    zoom.SetActive(false);
                    ayuda.SetActive(true);
                }
            }
            

        }
        else
        {
            comprar.SetActive(false);
            ayuda.SetActive(true);
        }
    }

    public void Ayuda()
    {
        if (compra)
        {
            if (!combina)
            {
                combinar.SetActive(true);
                ayuda.SetActive(false);
            }
            else
            {
                if (!venta)
                {
                    ayuda.SetActive(false);
                    vender.SetActive(true);
                }
                else
                {
                    zoom.SetActive(true);
                    ayuda.SetActive(false);
                }
            }

        }
        else
        {
            comprar.SetActive(true);
            ayuda.SetActive(false);
        }
    }

    IEnumerator Test()
    {
        yield return new WaitForSeconds(0.3f);

        if (!compra)
        {
            comprar.SetActive(true);
        }

        if (compra && !once)
        {
            comprar.SetActive(false);
            ayuda.SetActive(false);
            combinar.SetActive(true);
            once = true;
        }

        if (combina && !once1)
        {
            combinar.SetActive(false);
            vender.SetActive(true);
            ayuda.SetActive(false);
            once1 = true;
        }

        if (venta && !once2)
        {
            vender.SetActive(false);
            zoom.SetActive(true);
            yield return new WaitForSeconds(3f);
            empezar.SetActive(true);
            once2 = true;

            ayuda.SetActive(false);

        }

        if (zoom1)
        {
            zoom.SetActive(false);
            empezar.SetActive(false);
            oleada.GetComponent<OleadasController>().enabled = true;
            torre2.GetComponent<ComprarTorre2>().enabled = true;
        }
    }
}

