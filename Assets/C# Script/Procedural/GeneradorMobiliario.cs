using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorMobiliario : MonoBehaviour
{
    public GameObject[] tiles;

    public int[,] map = new int[25, 16]
    {
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2 ,0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 0 ,9},
        {0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 9, 0 ,9},
        {0, 9, 9, 9, 9, 9, 0, 0, 0, 0, 0, 0, 0, 9, 0 ,9},
        {6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 10, 9, 0 ,9},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 0 ,9},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0},
        {0, 10, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0},
        {9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0},
        {9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0},
        {9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0 ,0},
        {9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0},
        {9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0},
        {9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0},
        {6, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0 ,0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0},
        {0, 0, 5, 0, 0, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0 ,0},
        {0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0},
        {0, 0, 0, 0, 9, 9, 9, 9, 9, 0, 0, 0, 0, 0, 0 ,0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0 ,0},
    };

    // Start is called before the first frame update
    void Start()
    {
        GenerateMap();

    }

    // Update is called once per frame
    void Update()
    {
        //map.GetLength
    }

    private void GenerateMap()
    {
        for (int col = 0; col < map.GetLength(1); col++)
        {
            for (int row = 0; row < map.GetLength(0); row++)
            {
                GameObject clone = Instantiate(tiles[map[row, col]], transform.position, transform.rotation);
                clone.transform.position += new Vector3((row * 2), 0, (-col * 2));
            }
        }
    }
}
