using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraController : MonoBehaviour
{
    private Camera camara;

    public float velocidadMovimiento;
    public float velocidadZoom;
    public float FOVmin;
    public float FOVmax;

    public float x, z;

    public float xMin, xMax;
    public float zMin, zMax;

    private bool changeCursor = false;
    public Texture2D cursorDefecto;
    public Texture2D cursorDesplazamiento;
    private Vector3 camaraPosition;
    private float x1, x2, z1, z2;

    // Start is called before the first frame update
    void Start()
    {
        camara = GetComponent<Camera>();
        x1 = x;
        x2 = x;
        z1 = z;
        z2 = z;
    }

    // Update is called once per frame
    void Update()
    {
        CentrarCámara();

        float h = 0;
        float v = 0;


        if (camara.fieldOfView < FOVmax)
        {
            if (Input.mousePosition.x >= 0 && Input.mousePosition.x <= 20f)
            {
                v = -velocidadMovimiento * Time.deltaTime;
                Cursor.SetCursor(cursorDesplazamiento, Vector2.zero, CursorMode.Auto);
            }
            
            if (Input.mousePosition.x >= 20f && Input.mousePosition.x <= Screen.width - 20 && Input.mousePosition.y >= 20f && Input.mousePosition.y <= Screen.height - 20)
            {
                Cursor.SetCursor(cursorDefecto, Vector2.zero, CursorMode.Auto);
            }
            
            if (Input.mousePosition.x >= Screen.width - 20 && Input.mousePosition.x <= Screen.width)
            {
                v = velocidadMovimiento * Time.deltaTime;
                Cursor.SetCursor(cursorDesplazamiento, Vector2.zero, CursorMode.Auto);
            }

            if (Input.mousePosition.y >= 0 && Input.mousePosition.y <= 20f)
            {
                h = -velocidadMovimiento * Time.deltaTime;
                Cursor.SetCursor(cursorDesplazamiento, Vector2.zero, CursorMode.Auto);

            }
            
            if (Input.mousePosition.y >= Screen.height - 20 && Input.mousePosition.y <= Screen.height)
            {
                h = velocidadMovimiento * Time.deltaTime;
                Cursor.SetCursor(cursorDesplazamiento, Vector2.zero, CursorMode.Auto);
            }

            transform.position += new Vector3(v, 0, h);
        }

        var pos = camara.transform.position;
        pos.x = Mathf.Clamp(transform.position.x, x1, x2);
        pos.z = Mathf.Clamp(transform.position.z, z1, z2);
        transform.position = pos;

    }

    void LateUpdate()
    {
        camara.fieldOfView += Input.GetAxis("Mouse ScrollWheel") * -velocidadZoom;
        camara.fieldOfView = Mathf.Clamp(camara.fieldOfView, FOVmin, FOVmax);

        x1 -= Input.GetAxis("Mouse ScrollWheel") * 5;
        x1 = Mathf.Clamp(x1, xMin, x);


        z1 -= Input.GetAxis("Mouse ScrollWheel") * 5;
        z1 = Mathf.Clamp(z1, zMin, z);

        x2 += Input.GetAxis("Mouse ScrollWheel") * 5;
        x2 = Mathf.Clamp(x2, x, xMax);


        z2 += Input.GetAxis("Mouse ScrollWheel") * 5;
        z2 = Mathf.Clamp(z2, z, zMax);
    }

    void CentrarCámara()
    {
        Vector3 pos = transform.position;
        if (Input.GetKeyDown(KeyCode.Space))
        {
            camara.fieldOfView = FOVmax;
            pos.x = x;
            pos.z = z;
        }
        transform.position = pos;
    }
}
