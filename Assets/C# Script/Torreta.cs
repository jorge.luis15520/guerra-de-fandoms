using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torreta : MonoBehaviour
{
    [Header("General")]
     public float rango;

    [Header("Disparo normal")]
    public GameObject balaPrefab;
    public float velocidadAtaque;
    private float contador;

    [Header("Disparo Laser")]
    public bool torreLaser;
    public LineRenderer lineRenderer;

    [Header("Campos")]
    public Transform objetivo;
    public Transform parteRotar;
    public float velocidadRotación;
    public Transform puntoDisparo;

    private string tagEnemigo = "Enemigo";


    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("ActualizarObjetivo", 0f, 0.5f);
    }

    void ActualizarObjetivo()
    {
        GameObject[] enemigos = GameObject.FindGameObjectsWithTag(tagEnemigo);
        float distanciaMínima = Mathf.Infinity;
        GameObject enemigoCercano = null;

        foreach (GameObject enemigo in enemigos)
        {
            float distanciaDelEnemigo = Vector3.Distance(transform.position, enemigo.transform.position);
            if (distanciaDelEnemigo < distanciaMínima)
            {
                distanciaMínima = distanciaDelEnemigo;
                enemigoCercano = enemigo;
            }
        }

        if (enemigoCercano != null && distanciaMínima <= rango)
        {
            objetivo = enemigoCercano.transform;
        }
        else
        {
            objetivo = null;
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (objetivo == null)
        {
            if (torreLaser)
            {
                if (lineRenderer.enabled)
                {
                    lineRenderer.enabled = false;
                }
            }

            return;
        }

        BuscarObjetivo();

        if (torreLaser)
        {
            Laser();
        }
        else
        {
            if (contador <= 0f)
            {
                Disparo();
                contador = 1f / velocidadAtaque;
            }

            contador -= Time.deltaTime;
        }
    }

    void BuscarObjetivo()
    {
        Vector3 dirección = objetivo.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dirección);
        Vector3 rotación = Quaternion.Lerp(parteRotar.rotation, lookRotation, Time.deltaTime * velocidadRotación).eulerAngles;
        parteRotar.rotation = Quaternion.Euler(0f, rotación.y, 0f);
    }

    void Laser()
    {
        if (!lineRenderer.enabled)
        {
            lineRenderer.enabled = true;
        }

        lineRenderer.SetPosition(0, puntoDisparo.position);
        lineRenderer.SetPosition(1, objetivo.position);
    }
    void Disparo()
    {
        GameObject balaGO =  (GameObject)Instantiate(balaPrefab, puntoDisparo.position, puntoDisparo.rotation);
        Bala bala = balaGO.GetComponent<Bala>();

        if (bala != null)
        {
            bala.Buscar(objetivo);
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, rango);
    }
}
