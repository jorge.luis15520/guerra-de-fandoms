using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DefeatController : MonoBehaviour
{
    public GameObject panelDerrota;
    public GameObject transition;
    public GameObject baseJugador;
    public AudioClip clip;
    public bool derrota = false;
    private AudioSource audios;

    private void Update()
    {
        audios = GetComponent<AudioSource>();
        // CUANDO LA VIDA DE LA BASE SEA 0
        baseJugador = GameObject.FindGameObjectWithTag("Base");

        var vida = baseJugador.GetComponent<Base>().vida;

        if (vida <= 0 && !derrota)
        {
            DerrotaPanel();
            audios.PlayOneShot(clip);
            derrota = true;
        }

        if (vida > 0 && derrota)
        {
            derrota = false;
        }
    }

    public void DerrotaPanel()
    {
        panelDerrota.SetActive(true);
        Time.timeScale = 0f;
    }

    public void ResetearNivel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        panelDerrota.SetActive(false);
        Time.timeScale = 1f;

        //StartCoroutine(LoadScene(SceneManager.GetActiveScene().buildIndex));
    }

    public void IrAlMenu()
    {
        Time.timeScale = 1f;
        StartCoroutine(LoadScene(0));
    }

    IEnumerator LoadScene(int levelIndex)
    {
        transition.SetActive(true);
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(levelIndex);
    }

}
