using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class VentaTorres : MonoBehaviour, IPointerDownHandler
{
    public bool venderTorre;
    public GameObject HUD;
    private GameObject plataforma = null;
    private GameObject torreActual;
    private string nombre;
    public int tamaņoCursor = 32;
    public Texture2D cursorVender;
    public Texture2D cursorDefecto;
    public void OnPointerDown(PointerEventData eventData)
    {
        venderTorre = true;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (venderTorre)
        {
            Cursor.SetCursor(cursorVender, Vector2.zero, CursorMode.Auto);

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.gameObject.tag == "Plataforma")
                {
                    plataforma = hit.transform.gameObject;

                    if (plataforma != null)
                    {
                        torreActual = plataforma.GetComponent<Plataforma>().torre;

                        if (torreActual !=null)
                        {
                            nombre = torreActual.tag;
                        }
                    }

                    if (Input.GetMouseButtonUp(0) && torreActual != null)
                    {
                        VenderTorre();
                        venderTorre = false;
                        Cursor.SetCursor(cursorDefecto, Vector2.zero, CursorMode.Auto);

                    }
                }
            }

        }

        if (Input.GetMouseButtonUp(0))
        {
            venderTorre = false;
            Cursor.SetCursor(cursorDefecto, Vector2.zero, CursorMode.Auto);
        }


    }

    void VenderTorre()
    {
        switch (nombre)
        {
            case "Torreta 1":
                Destroy(plataforma.GetComponent<Plataforma>().torre);
                HUD.GetComponent<HUDModelo>().oro += 35;
                break;

            case "Torreta 2":
                Destroy(plataforma.GetComponent<Plataforma>().torre);
                HUD.GetComponent<HUDModelo>().oro += 70;
                break;

            case "Torreta 3":
                Destroy(plataforma.GetComponent<Plataforma>().torre);
                HUD.GetComponent<HUDModelo>().oro += 120;
                break;

            case "Torreta 4":
                Destroy(plataforma.GetComponent<Plataforma>().torre);
                HUD.GetComponent<HUDModelo>().oro += 200;
                break;

            default:
                break;
        }
    }
}
