using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    public GameObject menu;
    public GameObject resolution;
    public GameObject volver;
    public GameObject transition;
    public GameObject niveles;
    public GameObject loadingScreenTutorial;
    public GameObject loadingScreenNivel1;
    public GameObject loadingScreenNivel2;
    public GameObject loadingScreenNivel3;
    public GameObject loadingScreenNivel4;
    public GameObject loadingScreenNivel5;
    public GameObject creditScreen;

    public void Play()
    {
        loadingScreenTutorial.SetActive(true);
        StartCoroutine(LoadScreen(1));
    }
    
    IEnumerator LoadScreen(int levelIndex)
    {
        yield return new WaitForSeconds(3f);
        StartCoroutine(LoadScene(levelIndex));
    }

    public void Options()
    {
        menu.SetActive(false);
        resolution.SetActive(true);
        volver.SetActive(true);
    }

    public void EscogerNivel()
    {
        menu.SetActive(false);
        niveles.SetActive(true);
        volver.SetActive(true);
    }

    public void Salir()
    {
        Debug.Log("Holis");
        Application.Quit();
    }

    public void Volver()
    {
        menu.SetActive(true);
        resolution.SetActive(false);
        niveles.SetActive(false);
        volver.SetActive(false);
        creditScreen.SetActive(false);
    }

    IEnumerator LoadScene(int levelIndex)
    {
        transition.SetActive(true);
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(levelIndex);
    }

    public void IrNivel1()
    {
        loadingScreenNivel1.SetActive(true);
        StartCoroutine(LoadScreen(2));
    }

    public void IrNivel2()
    {
        loadingScreenNivel2.SetActive(true);
        StartCoroutine(LoadScreen(3));
    }

    public void IrNivel3()
    {
        loadingScreenNivel3.SetActive(true);
        StartCoroutine(LoadScreen(4));
    }

    public void IrNivel4()
    {
        loadingScreenNivel4.SetActive(true);
        StartCoroutine(LoadScreen(5));
    }

    public void IrNivel5()
    {
        loadingScreenNivel5.SetActive(true);
        StartCoroutine(LoadScreen(6));
    }

    public void IrCredits()
    {
        creditScreen.SetActive(true);
        volver.SetActive(true);
    }

}
