using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VictoryController : MonoBehaviour
{
    public static bool enPausa = false;
    public GameObject panelVictoria;
    public GameObject transition;
    public GameObject oleada;
    public List<GameObject> enemigos = new List<GameObject>();
    public int numeroOleadas;
    public bool victoria = false;
    private GameObject baseJugador;
    public GameObject loadScreen;
    public AudioClip clip;
    private AudioSource audios;

    private void Update()
    {

        audios = GetComponent<AudioSource>();
        // CUANDO TODOS LOS ENEMIGOS SEAN DERROTADOS

        baseJugador = GameObject.FindGameObjectWithTag("Base");

        var n = oleada.GetComponent<OleadasController>().oleadaActual;

        if (n == numeroOleadas - 1)
        {
            DetectarEnemigos();
        }

    }

    public void VictoryPanel()
    {
        panelVictoria.SetActive(true);
        Time.timeScale = 0f;
    }

    public void SiguienteNivel()
    {
        loadScreen.SetActive(true);
        Time.timeScale = 1f;
        //StartCoroutine(LoadScene(4));
        //Implementado en Niveles this:
        StartCoroutine(LoadScene(SceneManager.GetActiveScene().buildIndex + 1));
    }

    public void IrAlMenu()
    {
        Time.timeScale = 1f;
        StartCoroutine(LoadScene(0));
    }

    public void DetectarEnemigos()
    {
        enemigos.Clear();

        GameObject[] enemigosJuego = GameObject.FindGameObjectsWithTag("Enemigo");
        float v = baseJugador.GetComponent<Base>().vida;

        for (int i = 0; i < enemigosJuego.Length; i++)
        {
            enemigos.Add(enemigosJuego[i]);
        }

        if (enemigos.Count == 0 && !victoria && v != 0)
        {
            VictoryPanel();
            audios.PlayOneShot(clip);
            victoria = true;
        }
    }

    IEnumerator LoadScene(int levelIndex)
    {
        transition.SetActive(true);
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(levelIndex);
    }
}
