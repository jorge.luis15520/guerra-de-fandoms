using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPúblico : MonoBehaviour
{
    public List<GameObject> público = new List<GameObject>();
    public int tiempoEspera;
    public Transform puntoCreación;
    public bool wait = false;
    public int n;
    public float timer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (timer <= 0)
        {
            Spawn();
        }

        timer += Time.deltaTime;

        if (timer >= tiempoEspera)
        {
            timer = 0f;
        }
    }

    void Spawn()
    {
        n = Random.Range(0, público.Count);

        var p = Instantiate(público[n], puntoCreación.position, puntoCreación.rotation);

    }
}
