using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemigoModelo : MonoBehaviour
{
    [Header("Tipo Enemigo")]
    public bool enemigo1 = false;
    public bool enemigo2 = false;
    public bool enemigo3 = false;
    public bool jefe1;
    public bool jefe2;
    public bool jefe3;
    public float tiempoMax;
    public float tiempoEntre;

    [Header("Estad�sticas")]
    public float vidaInicial;
    public float vida;
    public float escudo;
    public float velocidad;
    public float velocidadInicial;
    public float velocidadRotacion;
    public int rango;
    public int da�o;

    [Header("Completar")]
    public GameObject escudoObject;
    public List<Transform> puntosRuta = new List<Transform>();
    public Image barraVida;
    public Canvas canvas;
    public GameObject efecto;
    public ParticleSystem buff;

    [Header("Auto")]
    public GameObject interfaz;
    public GameObject baseJugador;
    public AudioSource audioSource;
    public GameObject ruta1, ruta2, ruta3;
    public List<GameObject> enemigosenRango = new List<GameObject>();
    public TorreModelo torre;
    public Transform torreObjetivo;

    [Header("Variables")]
    public int objetivo = 1;
    public bool mover = true;
    public bool boostLanzado = false;
    public bool cambiodeVelocidad = false;
    public float timer;
    public bool afectado = false;
    public int count = 0;
    public float timer2;


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
