using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoVista : MonoBehaviour
{
    public EnemigoModelo modelo;

    private Transform t;
    private float fixedRotation = 0f;

    // Start is called before the first frame update
    void Start()
    {
        t = modelo.canvas.transform;
        modelo.vida = modelo.vidaInicial;
        modelo.canvas.enabled = false;
        modelo.escudoObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        t.eulerAngles = new Vector3(t.eulerAngles.x, fixedRotation, t.eulerAngles.z);


        if (modelo.enemigo3 && modelo.escudoObject != null)
        {
            modelo.escudoObject.SetActive(true);
        }
    }

    public void BarraVida()
    {
        modelo.canvas.enabled = true;

        modelo.barraVida.fillAmount = modelo.vida / modelo.vidaInicial;
    }

    public void Escudo()
    {
        if(modelo.escudo<=0)
        {
            Destroy(modelo.escudoObject);
        }
    }
}
