using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorreModelo : MonoBehaviour
{
    [Header("General")]
    public float rango;

    [Header("Disparo normal")]
    public GameObject balaPrefab;
    public float velocidadAtaque;
    public float contador;

    [Header("Disparo Laser")]
    public bool torreLaser;
    public int daño;
    public LineRenderer lineRenderer;
    public ParticleSystem efecto;

    [Header("Campos")]
    public Transform objetivo;
    public Transform parteRotar;
    public float velocidadRotación;
    public Transform puntoDisparo;

    public string tagEnemigo = "Enemigo";
    public EnemigoControlador enemigoObjetivo;
    public EnemigoModelo enemigoM;
    public float enemigoVelocidad;
    public float slow;

    public AudioSource audioSource;
    public AudioClip laser;

    
    public bool disparar = true;
    public float tiempomax;
    public float timer;

    public Material material;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
