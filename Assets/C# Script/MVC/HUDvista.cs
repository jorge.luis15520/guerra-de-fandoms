using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDvista : MonoBehaviour
{
    public HUDModelo modelo;
    
    // Start is called before the first frame update
    void Start()
    {
        modelo = GetComponent<HUDModelo>();
        modelo.oleadas = GameObject.FindGameObjectWithTag("Oleadas");

        modelo.vida2.SetActive(false);
        modelo.vida3.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        modelo.baseJugador = GameObject.FindGameObjectWithTag("Base");
        var num = modelo.oleadas.GetComponent<OleadasController>().oleada.Count;
        var num2 = modelo.oleadas.GetComponent<OleadasController>().oleadaActual;
        var vida = modelo.baseJugador.GetComponent<Base>().vida;

        modelo.oroTexto.text = "" + modelo.oro;
        modelo.oleadaTexto.text = "" + ((num - 1) - num2);

        if (vida <= 4)
        {
            modelo.vida1.SetActive(false);
            modelo.vida2.SetActive(true);
            modelo.vida3.SetActive(false);
        }

        if (vida <= 2)
        {
            modelo.vida1.SetActive(false);
            modelo.vida2.SetActive(false);
            modelo.vida3.SetActive(true);
        }
    }
}
