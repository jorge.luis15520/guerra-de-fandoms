using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorreControlador : MonoBehaviour
{
    public TorreModelo modelo;
    // Start is called before the first frame update
    
    
    void Start()
    {
        InvokeRepeating("ActualizarObjetivo", 0f, 0.5f);

        modelo.audioSource = GetComponent<AudioSource>();

        modelo.disparar = true;

        if (modelo.torreLaser)
        {
            modelo.audioSource.clip = modelo.laser;
        }
    }

    void ActualizarObjetivo()
    {
        GameObject[] enemigos = GameObject.FindGameObjectsWithTag(modelo.tagEnemigo);
        float distanciaMínima = Mathf.Infinity;
        GameObject enemigoCercano = null;

        foreach (GameObject enemigo in enemigos)
        {
            float distanciaDelEnemigo = Vector3.Distance(transform.position, enemigo.transform.position);
            if (distanciaDelEnemigo < distanciaMínima)
            {
                distanciaMínima = distanciaDelEnemigo;
                enemigoCercano = enemigo;
            }
        }

        if (enemigoCercano != null && distanciaMínima <= modelo.rango)
        {
            modelo.objetivo = enemigoCercano.transform;
            modelo.enemigoObjetivo = enemigoCercano.GetComponent<EnemigoControlador>();
            modelo.enemigoM = enemigoCercano.GetComponent<EnemigoModelo>();
        }
        else
        {
            modelo.objetivo = null;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (!modelo.disparar)
        {
            modelo.timer += Time.deltaTime;

            if (modelo.timer >= modelo.tiempomax)
            {
                modelo.timer = 0;
                modelo.disparar = true;
            }
        }

        if (modelo.objetivo == null)
        {
            if (modelo.torreLaser)
            {
                if (modelo.lineRenderer.enabled)
                {
                    modelo.lineRenderer.enabled = false;
                    modelo.efecto.Stop();
                }

                if (!modelo.lineRenderer.enabled && modelo.audioSource.isPlaying)
                {
                    modelo.audioSource.Stop();
                }

                if (modelo.lineRenderer.enabled && !modelo.disparar)
                {
                    modelo.lineRenderer.enabled = false;
                    modelo.efecto.Stop();

                    if (modelo.audioSource.isPlaying)
                    {
                        modelo.audioSource.Stop();
                    }
                }
            }

            return;
        }

        BuscarObjetivo();

        if (modelo.torreLaser)
        {
            if (modelo.lineRenderer.enabled && !modelo.disparar)
            {
                modelo.lineRenderer.enabled = false;
                modelo.efecto.Stop();

                if (modelo.audioSource.isPlaying)
                {

                    modelo.audioSource.Stop();
                }
            }
        }

        if (modelo.torreLaser && modelo.disparar)
        {
            Laser();
        }
        else
        {
            if (modelo.contador <= 0f)
            {
                if (modelo.disparar)
                {
                    Disparo();

                }
                modelo.contador = 1f / modelo.velocidadAtaque;
            }

            modelo.contador -= Time.deltaTime;
        }

        if (modelo.torreLaser)
        {
            if (modelo.lineRenderer.enabled && Time.timeScale == 0f && modelo.audioSource.isPlaying)
            {
                modelo.audioSource.Stop();
            }

            if (modelo.lineRenderer.enabled && Time.timeScale == 1f && !modelo.audioSource.isPlaying)
            {
                modelo.audioSource.Play();
            }
        }
    }

    void BuscarObjetivo()
    {
        Vector3 dirección = modelo.objetivo.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dirección);
        Vector3 rotación = Quaternion.Lerp(modelo.parteRotar.rotation, lookRotation, Time.deltaTime * modelo.velocidadRotación).eulerAngles;
        modelo.parteRotar.rotation = Quaternion.Euler(0f, rotación.y, 0f);
    }

    void Laser()
    {
        if (modelo.disparar)
        {
            modelo.enemigoObjetivo.RecibirDaño(modelo.daño * Time.deltaTime);
            modelo.enemigoObjetivo.Slow(modelo.slow);
            if (!modelo.objetivo.GetComponent<EnemigoModelo>().cambiodeVelocidad)
            {
                modelo.objetivo.GetComponent<EnemigoModelo>().cambiodeVelocidad = true;

            }

            if (!modelo.lineRenderer.enabled)
            {
                modelo.lineRenderer.enabled = true;
                modelo.enemigoM.cambiodeVelocidad = true;
                modelo.efecto.Play();
            }

            if (!modelo.audioSource.isPlaying)
            {
                modelo.audioSource.clip = modelo.laser;
                modelo.audioSource.Play();
            }

            if (modelo.audioSource.isPlaying && modelo.objetivo == null)
            {
                modelo.audioSource.Stop();
            }

            modelo.lineRenderer.SetPosition(0, modelo.puntoDisparo.position);
            modelo.lineRenderer.SetPosition(1, modelo.objetivo.position);

            Vector3 dir = modelo.puntoDisparo.position - modelo.objetivo.position;

            modelo.efecto.transform.position = modelo.objetivo.position + dir.normalized;
            modelo.efecto.transform.rotation = Quaternion.LookRotation(dir);
        }
    }
    void Disparo()
    {
        GameObject balaGO = (GameObject)Instantiate(modelo.balaPrefab, modelo.puntoDisparo.position, modelo.puntoDisparo.rotation);
        modelo.audioSource.Play();
        Bala bala = balaGO.GetComponent<Bala>();

        if (bala != null)
        {
            bala.Buscar(modelo.objetivo);
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, modelo.rango);
    }
}
