using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorreVista : MonoBehaviour
{
    public TorreModelo modelo;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (!modelo.disparar)
        {
            modelo.material.color = Color.black;
        }
        else
        {
            modelo.material.color = Color.white;
        }
    }
}
