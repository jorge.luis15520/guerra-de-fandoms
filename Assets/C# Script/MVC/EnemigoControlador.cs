using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemigoControlador : MonoBehaviour
{
    public EnemigoModelo modelo;
    public EnemigoVista vista;


    // Start is called before the first frame update
    void Start()
    {
        if (modelo.jefe3)
        {
            InvokeRepeating("DetectarTorre", 0f, 0.5f);
        }

        modelo.cambiodeVelocidad = false;
        modelo.audioSource = GetComponent<AudioSource>();
        modelo.interfaz = GameObject.FindGameObjectWithTag("HUD");
        modelo.velocidadInicial = modelo.velocidad;
        modelo.baseJugador = GameObject.FindGameObjectWithTag("Base");
    }

    private void Awake()
    {
        modelo.ruta1 = GameObject.FindGameObjectWithTag("Ruta1");
        modelo.ruta2 = GameObject.FindGameObjectWithTag("Ruta2");
        modelo.ruta3 = GameObject.FindGameObjectWithTag("Ruta3");

        if (modelo.ruta1 != null)
        {
            var distancia = Vector3.Distance(transform.position, modelo.ruta1.transform.position);

            if (distancia < 0.5f)
            {
                ObtenerRuta1();
            }
        }

        if (modelo.ruta2 != null)
        {
            var distancia = Vector3.Distance(transform.position, modelo.ruta2.transform.position);

            if (distancia < 0.5f)
            {
                ObtenerRuta2();
            }
        }

        if (modelo.ruta3 != null)
        {
            var distancia = Vector3.Distance(transform.position, modelo.ruta3.transform.position);

            if (distancia < 0.5f)
            {
                ObtenerRuta3();
            }
        }

        modelo.buff.Stop();
    }
    // Update is called once per frame
    void Update()
    {
        Movimiento();
        Girar();

        int n = modelo.puntosRuta.Count;

        if (transform.position == modelo.puntosRuta[n - 1].position)
        {
            Dañar();
        }

        if (modelo.jefe1 || modelo.jefe2)
        {
            DetectarEnemigos();

            modelo.timer += Time.deltaTime;


            if (modelo.jefe1)
            {
                
                BuffVelocidad();
            }

            if (modelo.jefe2)
            {
                BuffEscudo();
            }
        }

        if (modelo.jefe3)
        {
            if (modelo.torre !=null)
            {
                modelo.timer += Time.deltaTime;
                DebuffTorre();
            }
        }

        if (modelo.boostLanzado && modelo.count <2)
        {
            modelo.timer += Time.deltaTime;

            if (modelo.timer > modelo.tiempoEntre)
            {
                modelo.timer = 0f;

                modelo.boostLanzado = false;
            }
        }


        if (modelo.afectado && modelo.buff.isStopped)
        {

            modelo.buff.Play();

            modelo.afectado = false;
        }

        if (modelo.buff.isPlaying)
        {
            modelo.timer2 += Time.deltaTime;

            if (modelo.timer2 > 1.2f)
            {
                modelo.buff.Stop();
                modelo.timer2 = 0;
            }
        }

    }

    private void Movimiento()
    {
        
        transform.position = Vector3.MoveTowards(transform.position, modelo.puntosRuta[modelo.objetivo].position, modelo.velocidad * Time.deltaTime);
        var distancia = Vector3.Distance(transform.position, modelo.puntosRuta[modelo.objetivo].position);


        if (!modelo.audioSource.isPlaying && Time.timeScale == 1f)
        {
            modelo.audioSource.Play();
        }

        if (modelo.audioSource.isPlaying && Time.timeScale == 0f)
        {
            modelo.audioSource.Stop();
        }

        if (distancia <= 0.1f)
        {
            if (modelo.objetivo >= modelo.puntosRuta.Count - 1)
            {
                return;
            }

            modelo.objetivo++;
        }

        if (modelo.cambiodeVelocidad)
        {
            modelo.velocidad = modelo.velocidadInicial;
        }
    }

    private void Girar()
    {
        var dir = modelo.puntosRuta[modelo.objetivo].position - transform.position;

        var giro = Quaternion.LookRotation(dir);

        transform.rotation = Quaternion.Slerp(transform.rotation, giro, modelo.velocidadRotacion * Time.deltaTime);
    }
    public void Slow(float pct)
    {
        modelo.velocidad = modelo.velocidadInicial * (1f - pct);
    }

    private void ObtenerRuta1()
    {
        modelo.puntosRuta.Clear();

        var ruta = GameObject.Find("PuntosRuta1").transform;

        for (int i = 0; i < ruta.childCount; i++)
        {
            modelo.puntosRuta.Add(ruta.GetChild(i));
        }

        modelo.mover = true;
    }

    private void ObtenerRuta2()
    {
        modelo.puntosRuta.Clear();

        var ruta = GameObject.Find("PuntosRuta2").transform;

        for (int i = 0; i < ruta.childCount; i++)
        {
            modelo.puntosRuta.Add(ruta.GetChild(i));
        }

        modelo.mover = true;
    }

    private void ObtenerRuta3()
    {
        modelo.puntosRuta.Clear();

        var ruta = GameObject.Find("PuntosRuta3").transform;

        for (int i = 0; i < ruta.childCount; i++)
        {
            modelo.puntosRuta.Add(ruta.GetChild(i));
        }

        modelo.mover = true;
    }

    public void RecibirDaño(float daño)
    {
        if (modelo.enemigo3)
        {
            modelo.escudo -= daño;
            vista.Escudo();
            
            if(modelo.escudo <= 0)
            {

                modelo.vida -= daño;

                vista.BarraVida();

                if (modelo.vida <= 0)
                {
                    modelo.interfaz.GetComponent<HUDModelo>().oro += 10;
                    Destroy(gameObject);
                }
            }
        }
        else {
            modelo.vida -= daño;

            vista.BarraVida();

            if (daño >=1)
            {
                GameObject particulas = (GameObject)Instantiate(modelo.efecto, transform.position, transform.rotation);
                Destroy(particulas, 0.8f);
            }

            if (modelo.vida <= 0)
            {
                modelo.interfaz.GetComponent<HUDModelo>().oro += 10;
                Destroy(gameObject);
            }
        }
    }

    public void Dañar()
    {
        modelo.baseJugador.GetComponent<Base>().RecibirDaño(modelo.daño);
        Destroy(gameObject);
    }

    private void BuffVelocidad()
    {
        if (modelo.timer > modelo.tiempoMax && !modelo.boostLanzado)
        {
            modelo.timer = 0;

            for (int i = 0; i < modelo.enemigosenRango.Count; i++)
            {
                var n = modelo.enemigosenRango[i].GetComponent<EnemigoModelo>().cambiodeVelocidad;

                if (n)
                {
                    modelo.enemigosenRango[i].GetComponent<EnemigoModelo>().cambiodeVelocidad = false;
                }
                
                modelo.enemigosenRango[i].GetComponent<EnemigoModelo>().velocidad += 1;

                modelo.enemigosenRango[i].GetComponent<EnemigoModelo>().afectado = true;
            }

            modelo.boostLanzado = true;
            modelo.count++;

        }
    }

    private void BuffEscudo()
    {
        if (modelo.timer > modelo.tiempoMax && !modelo.boostLanzado)
        {
            modelo.timer = 0;

            for (int i = 0; i < modelo.enemigosenRango.Count; i++)
            {
                modelo.enemigosenRango[i].GetComponent<EnemigoModelo>().enemigo3 = true;
            }

            modelo.boostLanzado = true;

            modelo.count ++;
        }
    }

    private void DebuffTorre()
    {
        if (modelo.timer > modelo.tiempoMax && !modelo.boostLanzado)
        {
            modelo.timer = 0;
            modelo.torre.disparar = false;
            modelo.boostLanzado = true;
            modelo.count++;
        }

    }

    void DetectarEnemigos()
    {
        if (!modelo.boostLanzado)
        {
            modelo.enemigosenRango.Clear();


            GameObject[] enemigos = GameObject.FindGameObjectsWithTag("Enemigo");

            foreach (GameObject enemigo in enemigos)
            {
                float distanciaDelEnemigo = Vector3.Distance(transform.position, enemigo.transform.position);
                if (distanciaDelEnemigo < modelo.rango)
                {
                    modelo.enemigosenRango.Add(enemigo);
                }
            }
        }
    }

    void DetectarTorre()
    {
        if (!modelo.boostLanzado)
        {
            GameObject[] torres1 = GameObject.FindGameObjectsWithTag("Torreta 1");
            GameObject[] torres2 = GameObject.FindGameObjectsWithTag("Torreta 2");
            GameObject[] torres3 = GameObject.FindGameObjectsWithTag("Torreta 3");
            GameObject[] torres4 = GameObject.FindGameObjectsWithTag("Torreta 4");


            float distanciaMínima = Mathf.Infinity;
            GameObject torreCercano = null;

            foreach (GameObject torre in torres1)
            {
                float distancia= Vector3.Distance(transform.position, torre.transform.position);
                if (distancia < distanciaMínima)
                {
                    distanciaMínima = distancia;
                    torreCercano = torre;
                }
            }

            foreach (GameObject torre in torres2)
            {
                float distancia = Vector3.Distance(transform.position, torre.transform.position);
                if (distancia < distanciaMínima)
                {
                    distanciaMínima = distancia;
                    torreCercano = torre;
                }
            }

            foreach (GameObject torre in torres3)
            {
                float distancia = Vector3.Distance(transform.position, torre.transform.position);
                if (distancia < distanciaMínima)
                {
                    distanciaMínima = distancia;
                    torreCercano = torre;
                }
            }

            foreach (GameObject torre in torres4)
            {
                float distancia = Vector3.Distance(transform.position, torre.transform.position);
                if (distancia < distanciaMínima)
                {
                    distanciaMínima = distancia;
                    torreCercano = torre;
                }
            }

            if (torreCercano != null && distanciaMínima <= modelo.rango)
            {
                modelo.torreObjetivo = torreCercano.transform;
                modelo.torre = torreCercano.GetComponent<TorreModelo>();
            }
            else
            {
                modelo.torre = null;
            }
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, modelo.rango);
    }
}
